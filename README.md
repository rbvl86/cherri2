Cherri Ionic 2 Hybrid app
======

### What you need to run it / work on it

###### Editor
I highly recommend using [Visual Studio Code](https://code.visualstudio.com/) due to its nice features when it comes to TypeScript. It is free and available for all platforms.

You can use whatever code editor / IDE you like though!

###### Dependencies
Here is what you'll need to install in order to run Cherri locally (with the direct links to RTFM):

* [Git](https://git-scm.com/)
* [NodeJS abd NPM](https://nodejs.org/en/) node@v6.7.0 and npm@v3.10.3 (use nvm, see below)
* [nvm](https://github.com/creationix/nvm)
* [mongo](https://docs.mongodb.com/manual/administration/install-community/)
* [ionic 2](https://ionicframework.com/docs/v2/getting-started/installation/)
* [Java](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html)

### How to run it
* Clone repo
```
git clone git@bitbucket.org:rbvl86/cherri2.git
```
* Go to created directory
```
cd cherri2
```
* Run `ionic serve [-l]` `-l` is for **lab**, you'll get a preview of all 3 platforms, iOS, Android and Windows Phone

### How to build it
Run 
```
ionic build [platform]
```
`platform` can be `android`, `ios` or `windows`. Please note that

* In order to build for windows, you need a Windows PC
* In order to build for iOS, you need a Mac
* In order to build for Android, you need Java
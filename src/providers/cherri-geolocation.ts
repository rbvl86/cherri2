import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';

import { Logger } from '../utils/logger';

/*
  Generated class for the Geolocation provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriGeolocation {

  constructor(public http: Http) {
    
  }

  getLocation(latitude: number, longitude: number): Observable<any> {
    return this.http
      .get(`http://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}`)
      .map(this.extractLocation)
      .catch(this.handleError);
  }

  private extractLocation(res: Response) {
    let body = res.json();
    return body.results || {};
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

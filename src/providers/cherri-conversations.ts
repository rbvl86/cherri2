import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

import { AppConfig } from '../app.config';
import { Auth } from '../utils/auth';

/*
  Generated class for the CherriContacts provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriConversations {
  private userId;
  private headers = new Headers({ 'Authorization': Auth.Instance.Token });
  private reqOpts = new RequestOptions({ headers: this.headers });

  constructor(public http: Http) {
    console.log('Hello CherriContacts Provider');
  }

  public loadConversations(_id: any): Observable<any> {
    return this.http.get(`${AppConfig.API_URL}/users/${_id}/conversations`, this.reqOpts)
    .map((res: Response) => this.extractConversations(res))
  }

  private extractConversations(res: Response): any {
    return res.json();
  }

  public saveMessage(message: any, conversationId: any): Observable<any> {
    let reqBody = {
      message,
      conversationId
    }
    return this.http.post(`${AppConfig.API_URL}/messages`, reqBody, this.reqOpts)
      .map(res => res.json())
  }

}

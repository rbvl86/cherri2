import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { UserInfo } from '../utils/user';
import { Auth } from '../utils/auth';

import { AppConfig } from '../app.config';

/*
  Generated class for the CherriUser provider. Contains all http requests needed to get user info from SRV

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriUser {
  user; 
  reqOpts;

  constructor(public http: Http) {
    console.log('Hello CherriUser Provider');

    this.user = UserInfo.Instance.getUser();

    let headers = new Headers({ 'Authorization': Auth.Instance.Token });
    this.reqOpts = new RequestOptions({ headers });
  }

  getApps(): Observable<any> {
    return this.http.get(`${AppConfig.API_URL}/users/${this.user._id}/apps`, this.reqOpts)
      .map((res: Response) => this.extractApps(res));
  }

  private extractApps(res: Response): any {
    return res.json();
  }

  getFavs(): Observable<any> {
    return this.http.get(`${AppConfig.API_URL}/users/${this.user._id}/favs`, this.reqOpts)
      .map((res: Response) => this.extractFavs(res));
  }

  private extractFavs(res: Response): any {
    return res.json();
  }

  addFav(favId: any): Observable<any> {
    let reqBody = { favId };

    return this.http.post(`${AppConfig.API_URL}/users/${this.user._id}/favs`, reqBody, this.reqOpts);
  }
}

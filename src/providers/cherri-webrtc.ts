import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

import { Logger }  from '../utils/logger';

/*
  Generated class for the CherriWebrtc provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriWebrtc {

  constructor(public http: Http) {
    console.log('Hello CherriWebrtc Provider');
  }
  
  loadServers(): Observable<any> {
    let body = {
      ident: "rbvl",
      secret: "54139bda-883e-11e6-89d2-0b85197e7c2f",
      domain: "getcherri.com",
      application: "cherri",
      room: "cherri-room"
    }

    return this.http.post('https://service.xirsys.com/ice', body)
      .map((res: Response) => this.extractData(res))
      .catch(error => Logger.Instance.log(error))
  }

  private extractData(res: Response) {
    Logger.Instance.log(res.json());
    return res.json();
  }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AppConfig } from '../app.config';

/*
  Generated class for the CherriSignup provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriSignup {

  constructor(public http: Http) { }

  signup(username: string, email: string, password: string): Observable<any> {
    let reqBody = { username, email, password };

    return this.http
      .post(`${AppConfig.API_URL}/signup`, reqBody)
      .map((res: Response) => this.extractLoginData(res))
      .catch((error: any) => Observable.throw(error));
  }

  extractLoginData(res: Response) {
    return res.json();
  }

}

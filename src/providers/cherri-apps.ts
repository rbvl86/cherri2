import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

import { AppConfig } from '../app.config';
import { Auth } from '../utils/auth';

/*
  Generated class for the CherriApps provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriApps {

  constructor(public http: Http, public jsonp: Jsonp) {
    console.log('Hello CherriApps Provider');
  }

  extractBasicInfo(json: any, appname: string) {
    let body = json;
    let final = {
      username: 'No username found',
      picture: 'https://image.freepik.com/free-icon/profile-user-with-question-mark_318-41366.jpg'
    };
    switch (appname) {
      case 'Facebook':
        final.username = body.name;
        final.picture = body.picture.data.url;
        break;
      case 'Instagram':
        final.username = body.data.username;
        final.picture = body.data.profile_picture;
        break;
      case 'Spotify':
        final.username = body.display_name;
        final.picture = body.images[0].url;
        break;
      case 'Steam':
        final.username = body.response.players[0].personaname;
        final.picture = body.response.players[0].avatarfull;
        break;
    }
    console.log(final);
    return final;
  }

  getBasicInfo(app: any): Observable<any> {
    console.log('getBasicInfo for');
    console.log(app);
    // these come directly from server's User model
    let token = app.token;
    let id = app.id;
    // this will be set on the go
    let reqUrl, headers, reqOpts;

    function _jsonpcallback(res) {
      console.log('ok');
    }

    switch(app.appname) {
      case 'Facebook':
        reqUrl = `https://graph.facebook.com/v2.8/me?fields=id,name,picture.width(300).height(300)&access_token=${token}`;
        break;
      case 'Instagram':
        headers = new Headers({ 'Authorization': Auth.Instance.Token });
        reqOpts = new RequestOptions({ headers: headers }); 
        reqUrl = `${AppConfig.API_URL}/user/apps/instagram/basic?access_token=${app.token}`;
        break;
      case 'Spotify':
        headers = new Headers({ 'Authorization': `Bearer ${token}` });
        reqOpts = new RequestOptions({ headers: headers }); 
        reqUrl = `https://api.spotify.com/v1/me`;
        break;
      case 'Steam':
        headers = new Headers({ 'Authorization': Auth.Instance.Token });
        reqOpts = new RequestOptions({ headers: headers }); 
        reqUrl = `${AppConfig.API_URL}/user/apps/steam/basic?steamId=${app.id}`;
        break;
    }
    return this.http.get(reqUrl, reqOpts)
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .map((json) => {
        return this.extractBasicInfo(json, app.appname);
      });
  }

  getFacebookPictures(token: string) {
    return this.http.get(`https://graph.facebook.com/v2.8/me?fields=albums{photos{images},name},photos{images}&access_token=${token}`)
      .map((res) => { return res.json() });
  }

  getInstagramPictures(token: string) {
    let headers = new Headers({ 'Authorization': Auth.Instance.Token });
    let reqOpts = new RequestOptions({ headers: headers });
    
    return this.http.get(`${AppConfig.API_URL}/user/apps/instagram/full/?access_token=${token}`, reqOpts)
      .map((res) => { return res.json() });
  }

}

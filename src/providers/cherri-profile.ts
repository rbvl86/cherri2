import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Auth } from '../utils/auth';

import { AppConfig } from '../app.config';

/*
  Generated class for the CherriProfile provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriProfile {

  constructor(public http: Http) {
    console.log('Hello CherriProfile Provider');
  }

  loadProfile(id: string): Observable<any> {
    let headers = new Headers({ 'Authorization': Auth.Instance.Token });
    let reqOpts = new RequestOptions({ headers });

    return this.http.get(`${AppConfig.API_URL}/users/${id}`, reqOpts)
      .map(res => this.extractProfile(res));
  }

  private extractProfile(res: Response) {
    return res.json();
  }

}

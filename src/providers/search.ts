import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { User } from '../models/user';

import { AppConfig } from '../app.config';
import { Auth } from '../utils/auth';

/*
  Generated class for the Search provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Search {
  apiUrl = AppConfig.API_URL;

  constructor(public http: Http) { }

  public load(): Observable<User[]> {
    let headers = new Headers({ 'Authorization': Auth.Instance.Token });

    let reqOpts = new RequestOptions({ headers });

    return this.http.get(`${this.apiUrl}/users`, reqOpts)
      .map(res => <User[]>res.json());
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
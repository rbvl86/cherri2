import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { AppConfig } from '../app.config';
import { Logger } from '../utils/logger';

/*
  Generated class for the CherriLogin provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CherriLogin {

  constructor(public http: Http) { }

  login(username: string, password: string): Observable<any> {
    let reqBody = { username, password };

    return this.http
      .post(`${AppConfig.API_URL}/login`, reqBody)
      .map((res: Response) => this.extractLoginData(res))
      .catch((error: any) => Observable.throw(error));
  }

  extractLoginData(res: Response) {
    return res.json();
  }
}

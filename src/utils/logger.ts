import { Injectable } from '@angular/core';

@Injectable()
/**
 * Singleton class used to log in case of dev env
 */
export class Logger {
  private static instance: Logger;

  get log() {
    return console.log.bind(console);
  }

  get error() {
    return console.log.bind(console);
  }

  get debug() {
    return console.debug.bind(console);
  }

  static get Instance() {
    if (!this.instance) {
      this.instance = new Logger()
    }
    return this.instance;
  }
}
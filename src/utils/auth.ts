export class Auth {
  private static instance: Auth;
  private token: string;

  public static get Instance() {
    if (!this.instance) {
      this.instance = new Auth();
    }
    return this.instance;
  }

  public get Token(): string {
    return this.token;
  }

  public set Token(token: string) {
    this.token = token;
  }
}
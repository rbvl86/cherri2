import { Injectable } from '@angular/core';

import * as io from 'socket.io-client';

import { AppConfig } from '../app.config';

@Injectable()
export class Io {
socket: any;
ns: string;

  constructor(ns: string) {
    console.log(ns);
    this.socket = io.connect(`${AppConfig.API_URL}/${ns}`);
    console.log(this.socket);
  }

  get Socket(): any {
    return this.socket;
  }
}
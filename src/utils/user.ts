/**
 * This will be used to get all user info
 * and updated on every change made by them
 */
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
/**
 * Singleton class
 */
export class UserInfo {
  private storage: Storage;
  private static instance: UserInfo;
  private user;

  constructor() {
    this.storage = new Storage();
  }

  static get Instance() {
    if(!this.instance) {
      this.instance = new UserInfo;
    }
    return this.instance;
  }

  public getUser(): any {
    return this.user;
  }

  public setUser(user: any): Observable<any> {
    return Observable.create(observer => {
      this.user = user;
      this.storage.set('user', user);
      observer.next(user);
    });
  }

  getUserAttr(attr: string): any {
    if (!this.storage.get('user')) {
      return { error: "No user saved in device" };
    }

    try {
      return this.storage.get('user')[attr];
    } catch(error) {
      return { error };
    }
  }

  setUserAttr(attr: string, newValue: any): Object {
    let user = this.getUser();
    
    user[attr] = newValue;

    try {
      this.storage.set('user', user);
    } catch(error) {
      return { error };
    } finally {
      return { user };
    }
  }
}
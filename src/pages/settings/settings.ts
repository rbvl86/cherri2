import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SynclistPage } from '../synclist/synclist';

import { UserInfo } from '../../utils/user';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  user;

  constructor(public navCtrl: NavController) {
    
  }

  ionViewDidLoad() {
    console.log('Hello Settings Page');

    this.user = UserInfo.Instance.getUser();
  }

  goToSynclist() {
    this.navCtrl.push(SynclistPage);
  }

}

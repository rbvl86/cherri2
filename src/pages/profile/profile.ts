import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

import { CherriUser } from '../../providers/cherri-user';


/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [CherriUser]
})
export class ProfilePage {
  public profile;

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public userProvider: CherriUser,
    public toastCtrl: ToastController
    ) {
    this.profile = this.params.get('profile');
  }

  ionViewDidLoad() {
    console.log('Hello Profile Page');
  }

  addToFavs() {
    let toast = this.toastCtrl.create({ message: 'Added to favorites', duration: 2000, position: 'middle' });
    this.userProvider.addFav(this.profile._id).subscribe(
      res => { toast.present() }
    );
  }

  startConversation() {

  }

  reportOrBlock() {
    
  }

}

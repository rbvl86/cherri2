import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CherriApps } from '../../providers/cherri-apps';

/*
  Generated class for the Sync page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.

  This page is only displayed if sync is activated for the app
*/
@Component({
  selector: 'page-sync',
  templateUrl: 'sync.html',
  providers: [
    CherriApps
  ]
})
export class SyncPage {
  app;
  appInfo: any;

  constructor(
    public navCtrl: NavController, 
    public params: NavParams,
    public appsProvider: CherriApps
  ) {
    this.app = this.params.get('app');
  }

  ionViewDidLoad() {
    console.log('Hello Sync Page');
  }

  ionViewDidEnter() {
    console.log(this.app);
    this.appsProvider.getBasicInfo(this.app).subscribe(
      (res) => {
        this.appInfo = {
          username: res.username,
          picture: res.picture
        };
      },
      (error) => {
        console.log(error);
      }
    );
  }

}

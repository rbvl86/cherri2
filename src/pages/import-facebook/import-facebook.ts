import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CherriApps } from '../../providers/cherri-apps';

/*
  Generated class for the ImportFacebook page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-import-facebook',
  templateUrl: 'import-facebook.html',
  providers: [
    CherriApps
  ]
})
export class ImportFacebookPage {
  private fbInfo: any;
  albums;
  photos;
  loaded: boolean = false;

  constructor(public navCtrl: NavController, public params: NavParams, public appsProvider: CherriApps) {}

  ionViewDidLoad() {
    console.log('Hello ImportFacebook Page');
  }

  ionViewDidEnter() {
    this.fbInfo = this.params.get('fbInfo');
    this.appsProvider.getFacebookPictures(this.fbInfo.token).subscribe(
      (res) => {
        console.log(res);
        this.albums = res.albums.data;
        this.photos = res.photos.data;
        this.loaded = true;
      }
    )
  }

}

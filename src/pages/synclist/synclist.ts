import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

import { Io } from '../../utils/io';
import { Auth } from '../../utils/auth';
import { UserInfo } from '../../utils/user';

import { SyncPage } from '../sync/sync';

import { AppConfig } from '../../app.config';

import { CherriUser } from '../../providers/cherri-user';

/*
  Generated class for the Synclist page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-synclist',
  templateUrl: 'synclist.html',
  providers: [
    CherriUser
  ]
})
export class SynclistPage {
  socket = new Io('sync').Socket;
  apps = null;
  appsArray;
  user;

  constructor(public navCtrl: NavController, public userProvider: CherriUser) {}

  ionViewDidLoad() {
    console.log('Hello Synclist Page');
  }

  ionViewDidEnter() {
    let self = this;
    this.user = UserInfo.Instance.getUser();
    this.socket.emit('appconnection', this.user._id);
    this.socket.on('appconnected', (message) => {
      console.log('appconnected');
      self.getUserApps();
    });
    self.getUserApps();
  }

  goToSyncPage(app: any) {
    console.log(app);
    if (this.apps[app].token) {
      this.navCtrl.push(SyncPage, { app: this.apps[app] });
    } else {
      let browser = new InAppBrowser(`${AppConfig.API_URL}/connect/${app}?tk=${Auth.Instance.Token}`, '_self');
    }
  }

  getUserApps() {
    this.userProvider.getApps().subscribe(
      (apps) => { 
        this.apps = apps;
        this.appsArray = Object.keys(this.apps);
        console.log(this.appsArray);
      }
    );
  }

}

import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { SetupPage } from '../setup/setup';

import { CherriSignup } from '../../providers/cherri-signup';

import { UserInfo } from '../../utils/user';
import { Logger } from '../../utils/logger';
import { Auth } from '../../utils/auth';

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  providers: [
    CherriSignup
  ]
})
export class SignupPage {
  username: string = '';
  email: string = '';
  password: string = '';

  constructor(public navCtrl: NavController, public signupProvider: CherriSignup, public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('Hello Signup Page');
  }

  goToLogin() {
    this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }

  signup(username: string, email: string, password: string) {
    this.signupProvider.signup(username, email, password).subscribe(
      res => {
        Logger.Instance.log(res);
        if (res) {
          if (res.success) {
            UserInfo.Instance.setUser(res.user).subscribe(
              user => {
                Auth.Instance.Token = res.token;
                this.navCtrl.setRoot(SetupPage);
              }
            );
          } else {
            let errorAlert = this.alertCtrl.create({
              title: 'Login Error',
              subTitle: res.msg,
              buttons: ['OK']
            });
            errorAlert.present();
          }
        }
      },
      error => { Logger.Instance.log(error) }
    );
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { VideoPage } from '../video/video';

import { CherriConversations } from '../../providers/cherri-conversations';

import { Io } from '../../utils/io';

import { AppConfig } from '../../app.config';
import { UserInfo } from '../../utils/user';
import { Logger } from '../../utils/logger';

/*
  Generated class for the Chat page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
  providers: [CherriConversations]
})
export class ChatPage {
  socket = new Io('chat').Socket;
  // our socket
  socketId: string;
  // our friend's socket
  peerId: string;

  public user;
  public contact: any;
  public conversationId: any;
  public messages = [];

  message: string = '';

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public modalCtrl: ModalController,
    public conversationProvider: CherriConversations) {

        this.user = UserInfo.Instance.getUser();
        this.conversationId = params.get('conversationId');
        this.contact = params.get('contact');
        this.messages = params.get('messages');
        this.socketId = this.user._id + this.conversationId;
        this.peerId = this.contact._id + this.conversationId;

  }

  ionViewDidLoad() {
    console.log('Hello Chat Page');
  }

  ionViewDidEnter() {
    this.socket.emit('subscribe to conversation', this.socketId);

    this.socket.on('chatMessageReceived', (sender, message) => {
      console.log('chat message received');
      this.messages.push(message);
    });

    this.socket.on('videoMessage', (sender, callParams) => {
      if (sender !== this.socketId) {
        let cParams = JSON.parse(callParams);
        (cParams.type === 'call') && this.showVideoModal(cParams.video, true);
      }
    });
  }

  sendMessage() {
    let msg = {
      from: this.user._id,
      to: this.contact._id,
      content: this.message
    }
    this.socket.emit('chatMessage', this.peerId, msg);

    this.conversationProvider.saveMessage(msg, this.conversationId).subscribe(
      message => {
        this.messages.push(message);
        this.message = '';
      }
    );
  }

  showVideoModal(video: boolean, isCallee: boolean) {
    console.log('open modal');
    try {
      let videoModal = this.modalCtrl.create(VideoPage, {
        socket: this.socket,
        socketId: this.socketId,
        contact: this.contact,
        peerId: this.peerId,
        isCallee,
        video
      })
      videoModal.present();
    } catch (error) {
      console.log(error);
    }
  }
}

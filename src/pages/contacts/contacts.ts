import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CherriConversations } from '../../providers/cherri-conversations';
import { CherriUser } from '../../providers/cherri-user';

import { ChatPage } from '../chat/chat';
import { ProfilePage } from '../profile/profile';

import { UserInfo } from '../../utils/user';

/*
  Generated class for the Contacts page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
  providers: [
    CherriConversations,
    CherriUser,
    UserInfo
  ]
})
export class ContactsPage {
  conversations = [];
  favs = [];
  user: any;
  content: string = 'conversationSegment';

  constructor(
    public navCtrl: NavController,
    public conversationsProvider: CherriConversations,
    public userProvider: CherriUser
  ) { }

  ionViewDidLoad() {
    console.log('Hello Contacts Page');
  }

  ionViewDidEnter() {
    this.user = UserInfo.Instance.getUser();
    this.getFavs();
    this.getContacts(this.user._id);
  }

  getContacts(_id: any) {
    this.conversationsProvider.loadConversations(_id).subscribe(
      conversations => {
        this.conversations = conversations;
        let now = new Date();
        console.log(conversations);

        for (let i in conversations) {
          let self = this;
          let lastMessage = conversations[i].messages[conversations[i].messages.length - 1];
          let lastMessageDate = new Date(lastMessage.sent_at);
          let sameDay = now.getDate() === lastMessageDate.getDate()
            && now.getMonth() === lastMessageDate.getMonth()
            && now.getFullYear() === lastMessageDate.getFullYear();

          conversations[i].lastMessage = {
            date: sameDay ? lastMessageDate.toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' }) : lastMessageDate.toLocaleDateString(),
            content: lastMessage.content
          }

          conversations[i].goToConversation = function () {
            self.navCtrl.push(ChatPage, {
              conversationId: conversations[i]._id,
              contact: conversations[i].users[0],
              messages: conversations[i].messages,
              userId: self.user._id
            });
          }
        }
      }
    );
  }

  private getFavs() {
    this.userProvider.getFavs().subscribe(
      favs => {
        this.favs = favs;
        console.log(this.favs);
      }
    )
  }

  goToProfile(profile: any) {
    this.navCtrl.push(ProfilePage, { profile })
  }

}

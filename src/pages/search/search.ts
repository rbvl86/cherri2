import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { Search } from '../../providers/search';

import { ProfilePage } from '../profile/profile';

import { User } from '../../models/user';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
  providers: [
    Search,
    ProfilePage
  ]
})
export class SearchPage {
  errorMessage = '';
  users: User[];

  constructor(public navCtrl: NavController, public search: Search) {
    search.load().subscribe(
      users => {
        this.users = users;
        console.log(users);
      },
      error => this.errorMessage = <any>error
    );
  }

  viewProfile(profile: any) {
    this.navCtrl.push(ProfilePage, { profile });
  }

}

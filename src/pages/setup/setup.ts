import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';
import { InAppBrowser, DatePicker, Camera, Geolocation } from 'ionic-native';
import * as _ from 'lodash';

import { TabsPage } from '../tabs/tabs';
import { ImportFacebookPage } from '../import-facebook/import-facebook';
import { ImportInstagramPage } from '../import-instagram/import-instagram';

import { CherriGeolocation } from '../../providers/cherri-geolocation';

import { Auth } from '../../utils/auth';
import { Io } from '../../utils/io';
import { UserInfo } from '../../utils/user';

import { AppConfig } from '../../app.config';

/*
  Generated class for the Setup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
  providers: [
    CherriGeolocation
  ]
})
export class SetupPage {
  swiper: any;
  currentIndex: number = 0;
  private socket = new Io('sync').Socket;
  user: any;
  fbInfo: any = null;
  instagramInfo: any = null;

  userData = {
    gender: null,
    orientation: null,
    age_range: {
      lower: 25,
      upper: 35  
    },
    location: {
      country: '',
      city: 'a',
      postcode: '',
      latitude: null,
      longitude: null,
    },
    birthdate: new Date(),
    loves: [
      {id: null, value: 'cdscds'},
      {id: null, value: 'cdscdssdc'},
      {id: null, value: 'd'},
      {id: null, value: ''},
      {id: null, value: ''}
    ],
    hates: [
      {id: null, value: ''},
      {id: null, value: ''},
      {id: null, value: ''},
      {id: null, value: ''},
      {id: null, value: ''}
    ],
    whatifpos: [
      {value: ''},
      {value: ''},
      {value: ''}
    ],
    whatifneg: [
      {value: ''},
      {value: ''},
      {value: ''}
    ],
    flaw: '',
    tweet: '',
    avatarSource: null
  }

  @ViewChild('slider') slider: Slides;

  sliderOptions = {
    pager: true
  }

  constructor(public navCtrl: NavController, public geolocationProvider: CherriGeolocation) {}

  ionViewDidLoad() {
    let self = this;
    this.user = UserInfo.Instance.getUser();

    // this should not happen often, but let's check if user already authorised FB or instagram
    this.user.apps.facebook.token && (this.fbInfo = this.user.apps.facebook);
    this.user.apps.instagram.token && (this.instagramInfo = this.user.apps.instagram);

    this.socket.emit('appconnection', this.user._id);
    this.socket.on('appconnected', (message) => {
      console.log(message);
      if (message.app === 'facebook') {
        self.fbInfo = message;
        self.goToFacebookAlbums();
      }
      if (message.app === 'instagram') {
        self.instagramInfo = message;
        self.goToInstagramPictures();
      }
    });
  }

  ionViewDidEnter() {
    console.log(this.user);
  }

  onIonDrag(event) {
    this.swiper = event;
    event.lockSwipes();
  }

  goTo(direction: number) {
    if (this.swiper) {
      this.swiper.unlockSwipes();
    }
    this.slider.slideTo(this.slider.getActiveIndex() + direction, 300, true);
  }

  onSlideChange() {
    this.currentIndex = this.slider.getActiveIndex();
  }

  save() {
    //save info here
    console.log(this.userData);
    // this.navCtrl.setRoot(TabsPage);
  }

  selectGender(gender: string): void {
    this.userData.gender = gender;
  }

  selectOrientation(orientation: string): void {
    this.userData.orientation = orientation;
  }

  locate(): void {

  }

  showDatePicker(): void {
    let now = new Date();
    let eighteenYearsBefore = new Date();
    eighteenYearsBefore.setFullYear(now.getFullYear() - 18);

    DatePicker.show({
      mode: 'date',
      date: new Date(),
      maxDate: eighteenYearsBefore,
      androidTheme: 2,
      titleText: 'Birth Date'
    }).then(
      date => this.userData.birthdate = date,
      err => console.log(err)
    );
  }

  getLocation() {
    let self = this;

    Geolocation.getCurrentPosition().then(res => {
      self.userData.location.latitude = res.coords.latitude;
      self.userData.location.longitude = res.coords.longitude;

      self.geolocationProvider.getLocation(res.coords.latitude, res.coords.longitude).subscribe(res => {
        console.log(res);
        let address = res[1];
        address.address_components.forEach(component => {
          if (_.includes(component.types, 'postal_code')) {
            self.userData.location.postcode = component.short_name;
          }
          if (_.includes(component.types, 'country')) {
            self.userData.location.country = component.long_name;
          }
          if (_.includes(component.types, 'locality') || _.includes(component.types, 'administrative_area_level_2')) {
            self.userData.location.city = component.long_name;
          }
        });
      })
    }).catch(error => {
      console.log('Error getting location', error);
    })
  }
  
  connectFacebook() {
    if(this.fbInfo) {
      this.goToFacebookAlbums();
    } else {
      let browser = new InAppBrowser(`${AppConfig.API_URL}/connect/facebook?tk=${Auth.Instance.Token}`, '_self');
    }
  }

  connectInstagram() {
    if(this.instagramInfo) {
      this.goToInstagramPictures();
    } else {
      let browser = new InAppBrowser(`${AppConfig.API_URL}/connect/instagram?tk=${Auth.Instance.Token}`, '_self');
    }
  }

  goToSearch(): void {
    this.navCtrl.setRoot(TabsPage);
  }

  goToFacebookAlbums() {
    this.navCtrl.push(ImportFacebookPage, { fbInfo: this.fbInfo })
  }

  goToInstagramPictures() {
    this.navCtrl.push(ImportInstagramPage, { instagramInfo: this.instagramInfo });
  }

  openCamera(sourceType: number) {
    let self = this;

    Camera.getPicture({
      quality: 100,
      allowEdit: true,
      sourceType,
      mediaType: 0,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      cameraDirection: 0
    }).then(
      (fileUri) => { self.userData.avatarSource = fileUri; }
    )
  }

}

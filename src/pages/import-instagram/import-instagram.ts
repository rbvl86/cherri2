import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CherriApps } from '../../providers/cherri-apps';

/*
  Generated class for the ImportInstagram page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-import-instagram',
  templateUrl: 'import-instagram.html',
  providers: [
    CherriApps
  ]
})
export class ImportInstagramPage {
  private instagramInfo: any;
  pictures: Array<Object>;

  constructor(public navCtrl: NavController, public params: NavParams, public appsProvider: CherriApps) {}

  ionViewDidLoad() {
    console.log('Hello ImportInstagram Page');
  }

  ionViewDidEnter() {
    this.instagramInfo = this.params.get('instagramInfo');
    console.log(this.instagramInfo);
    this.appsProvider.getInstagramPictures(this.instagramInfo.token).subscribe(
      (res) => {
        console.log(res);
        this.pictures = res.data;
      }
    )
  }

}

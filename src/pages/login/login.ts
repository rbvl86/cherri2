import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';
import { SetupPage } from '../setup/setup';

import { CherriLogin } from '../../providers/cherri-login';

import { UserInfo } from '../../utils/user';
import { Logger } from '../../utils/logger';
import { Auth } from '../../utils/auth';
import { AppConfig } from '../../app.config';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [
    CherriLogin
  ]

})
export class LoginPage {
  username: string = 'user1';
  password: string = 'password';
  errorMessage: string;

  constructor(public navCtrl: NavController, public loginProvider: CherriLogin, public alertCtrl: AlertController) { }

  ionViewDidLoad() {
    console.log('Hello Login Page');
  }

  goToSignup() {
    this.navCtrl.setRoot(SignupPage, {}, { animate: true, direction: 'back' });
  }

  login(username: string, password: string) {
    this.loginProvider.login(username, password).subscribe(
      res => {
        Logger.Instance.log(res);
        if (res) {
          if (res.success) {
            UserInfo.Instance.setUser(res.user).subscribe(
              user => {
                Auth.Instance.Token = res.token;
                this.navCtrl.setRoot(SetupPage);
              }
            );
          } else {
            let errorAlert = this.alertCtrl.create({
              title: 'Login Error',
              subTitle: res.msg,
              buttons: ['OK']
            });
            errorAlert.present();
          }
        }
      },
      error => { Logger.Instance.log(error) }
    );
  }

  setApiUrl(url: string) {
    AppConfig.API_URL = url;
    console.log(AppConfig.API_URL);
  }
}

import { Component } from '@angular/core';

import { ContactsPage } from '../contacts/contacts';
import { SearchPage } from '../search/search';
import { UserPage } from '../user/user';

import { UserInfo } from '../../utils/user';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = ContactsPage;
  tab2Root: any = SearchPage;
  tab3Root: any = UserPage;

  user;

  constructor() {
    this.user = UserInfo.Instance.getUser();
  }
}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { UserInfo } from '../../utils/user';

import { Io } from '../../utils/io';
import { CherriWebrtc } from '../../providers/cherri-webrtc';

import { Logger } from '../../utils/logger';

/*
  Generated class for the Video page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
/// <reference path="../../utils/RTCygygPeerConnection.d.ts" />
/// <reference path="../../utils/MediaStream.d.ts" />
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
  providers: [
    CherriWebrtc
  ]
})
export class VideoPage {
  private socket = new Io('chat').Socket;
  private socketId;
  private peerId;
  contact;
  isCallee;

  public localVideo: any;
  public remoteVideo: any;
  public localStream: MediaStream;
  public constraints;
  public peerConnection;
  private peerConnectionConfig;

  constructor(public navCtrl: NavController, public webrtc: CherriWebrtc, public params: NavParams) {
    this.constraints = {
      audio: true,
      video: params.get('video')
    };
    this.socket = params.get('socket');
    this.socketId = params.get('socketId');
    this.contact = params.get('contact');
    this.peerId = params.get('peerId');
    this.isCallee = params.get('isCallee');
  }

  ionViewDidLoad() {
    console.log('Hello Video Page');
  }

  ionViewDidEnter() {
    this.localVideo = document.querySelector('#localVideo');
    this.remoteVideo = document.querySelector('#remoteVideo');
    this.getPeerConnectionConfig();
    // used for listening to socket below
    let self = this;

    if (navigator.getUserMedia) {
      navigator.getUserMedia(this.constraints,
        stream => this.getUserMediaSuccess(stream),
        error => this.getUserMediaError(error));
    }

    // set socket listener
    // use self cos we're inside Socket right now
    this.socket.on('videoMessage', (peer: any, message: any) => {
      let signal = JSON.parse(message);
      Logger.Instance.log(signal);

      switch (signal.type) {
        case 'callYes':
          self.peerConnection = new webkitRTCPeerConnection(self.peerConnectionConfig);
          self.peerConnection.onicecandidate = function(event) {
            if (event.candidate !== null) {
              self.socket.emit('videoMessage', self.peerId, JSON.stringify({ 'type': 'ice', 'ice': event.candidate }));
            }
          };
          self.peerConnection.onaddstream = function(event) {
            self.remoteVideo.src = window.URL.createObjectURL(event.stream);
          };
          self.peerConnection.addStream(self.localStream);
          break;
        case 'callNo': {
          self.localStream.getTracks().forEach(track => {
            track.stop();
          })
          self.navCtrl.pop();
        }
        case 'endCall': {
          self.localStream.getTracks().forEach(track => {
            track.stop();
          })
          self.navCtrl.pop();
          // TODO display toast here
        }
        // TODO Would it be feasible to do this externally?
        case 'sdp':
          self.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), () => {
            self.peerConnection.createAnswer((description: any) => {
              self.peerConnection.setLocalDescription(description, () => {
                self.socket.emit('videoMessage', self.peerId, JSON.stringify({ 'type': 'sdp', 'sdp': description }));
              })
            }, self.createAnswerError, { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': self.params.get('video') } });
          }, (error) => {
            Logger.Instance.error('setRemoteDescription Error');
            Logger.Instance.error(error);
          });
          break;
        case 'ice':
          self.peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice));
          break;
        case 'peeroffline':
          alert(self.contact.local.username + ' seems to be offline');
          break;
        case 'useroffline':
          alert('There was an issue while making your call, could you please try again?');
          break;
      }
    });
  }
  private getPeerConnectionConfig() {
    this.webrtc.loadServers().subscribe(
      res => { this.peerConnectionConfig = res.d },
      error => { Logger.Instance.log(error) }
    );
  }

  private getUserMediaSuccess(stream: any): void {
    this.localStream = stream;
    this.localVideo.src = window.URL.createObjectURL(stream);
  }

  private getUserMediaError(error: any): void {
    Logger.Instance.log(error);
  }

  call() {
    Logger.Instance.log('call');
    this.socket.emit('videoMessage', this.peerId, JSON.stringify({ 'type': 'call', 'video': this.params.get('video') }));
  }

  answer() {
    this.socket.emit('videoMessage', this.peerId, JSON.stringify({ 'type': 'callYes' }));
    this.start(true);
  }

  ignore() {
    this.socket.emit('videoMessage', this.peerId, JSON.stringify({ 'type': 'callNo' }));
  }

  start(isCallee: boolean) {
    let self = this;
    self.peerConnection = new webkitRTCPeerConnection(self.peerConnectionConfig);
    self.peerConnection.onicecandidate = function(event: any) {
      if (event.candidate !== null) {
        self.socket.emit('videoMessage', self.peerId, JSON.stringify({ 'type': 'ice', 'ice': event.candidate }));
      }
    };
    self.peerConnection.onaddstream = function(event: any) {
      self.remoteVideo.src = window.URL.createObjectURL(event.stream);
    };
    self.peerConnection.addStream(self.localStream);

    if (isCallee) {
      self.peerConnection.createOffer((description: any) => {
        self.peerConnection.setLocalDescription(description, () => {
          self.socket.emit('videoMessage', self.peerId, JSON.stringify({ 'type': 'sdp', 'sdp': description }));
        });
      }, self.createOfferError, 
        { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': self.params.get('video') } });
    }
  }

  createOfferError(error: any) {
    Logger.Instance.error('createOfferError');
    Logger.Instance.error(error);
  }

  createAnswerError(error) {
    Logger.Instance.error('createAnswerError');
    Logger.Instance.error(error);
  }

  endCall() {
    let self = this;
    self.localStream.getTracks().forEach(track => {
      track.stop();
    });
    self.socket.emit('videoMessage', self.peerId, JSON.stringify({ 'type': 'endCall' }));
    self.navCtrl.pop();
  }

}
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SettingsPage } from '../settings/settings';

import { CherriGeolocation } from '../../providers/cherri-geolocation';

/*
  Generated class for the User page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
  providers: [
    CherriGeolocation
  ]
})
export class UserPage {
  mode = 'Observable';
  // location to be displayed on-screen
  friendlyLocation: string;
  errorMessage;

  constructor(public navCtrl: NavController, private geoloc: CherriGeolocation) { }

  ionViewDidLoad() {
    console.log('Hello User Page');
  }

  getGeolocation() {
    
  }

  goToSettings() {
    this.navCtrl.push(SettingsPage)
  }

}

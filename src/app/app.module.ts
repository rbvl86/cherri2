import { NgModule } from '@angular/core';
import { JsonpModule } from '@angular/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Cherri } from './app.component';

import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';

import { SearchPage } from '../pages/search/search';
import { ContactsPage } from '../pages/contacts/contacts';
import { UserPage } from '../pages/user/user';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile'
import { ChatPage } from '../pages/chat/chat';
import { VideoPage } from '../pages/video/video';
import { SettingsPage } from '../pages/settings/settings';
import { SynclistPage } from '../pages/synclist/synclist';
import { SyncPage } from '../pages/sync/sync';
import { SetupPage } from '../pages/setup/setup';
import { ImportFacebookPage } from '../pages/import-facebook/import-facebook';
import { ImportInstagramPage } from '../pages/import-instagram/import-instagram';

@NgModule({
  declarations: [
    Cherri,
    LoginPage,
    SignupPage,
    SearchPage,
    ContactsPage,
    UserPage,
    TabsPage,
    ProfilePage,
    ChatPage,
    VideoPage,
    SettingsPage,
    SynclistPage,
    SyncPage,
    SetupPage,
    ImportFacebookPage,
    ImportInstagramPage
  ],
  imports: [
    IonicModule.forRoot(Cherri),
    JsonpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Cherri,
    LoginPage,
    SignupPage,
    SearchPage,
    ContactsPage,
    UserPage,
    TabsPage,
    ProfilePage,
    ChatPage,
    VideoPage,
    SettingsPage,
    SynclistPage,
    SyncPage,
    SetupPage,
    ImportFacebookPage,
    ImportInstagramPage
  ],
  providers: []
})
export class AppModule {}

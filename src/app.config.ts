export class AppConfig {
  static url = 'http://localhost:8080';

  public static get API_URL(): string {
    return this.url;
  }

  public static set API_URL(url: string) {
    this.url = url;
  }

  private static get WEBRTC_SECRET(): string { return '54139bda-883e-11e6-89d2-0b85197e7c2f' }
}
export interface User {
    local: {
        username: string
    },
    facebook: {
        token: string
    },
    spotify: {
        token: string
    },
    location: {
        display: string
    }
}